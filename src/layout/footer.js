import React from 'react';

import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

function Footer() {

return(

    <Container>
        Copyright © Designed &amp; Developed by <a href="http://designer-dev.com/">Designer Dev</a> .
    </Container>
 );

}

export default Footer;