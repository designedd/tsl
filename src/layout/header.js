import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import PropTypes from 'prop-types';



function Item(props) {
    const { sx, ...other } = props;
    return (
      <Box
        sx={{
          bgcolor: 'primary.main',
          color: 'white',
          p: 1,
          m: 1,
          borderRadius: 1,
          textAlign: 'center',
          fontSize: '1rem',
          fontWeight: '700',
          ...sx,
        }}
        {...other}
      />
    );
  }
  
  Item.propTypes = {
    sx: PropTypes.oneOfType([
      PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.bool]),
      ),
      PropTypes.func,
      PropTypes.object,
    ]),
  };

function Header() {

return(

    <Box>
        <Grid container spacing={2} >
       <Grid item lg={3}>
            logo
       </Grid>
       <Grid item item lg={6}>
       <Box sx={{ display: 'grid', gridTemplateColumns: 'repeat(4, 1fr)' }}>
       <Item>Hotel</Item>
       <Item>Flight</Item>
       <Item>Cars</Item>
       <Item>tours</Item>
       </Box>
       </Grid>
       <Grid item item lg={3}>
            auth
       </Grid>
       </Grid>

    </Box>
 );

}

export default Header;