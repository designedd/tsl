import React from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';




import Header from './layout/header';
import Footer from './layout/footer';
import Siderbar from './layout/sidebar';
import Route from './route';


/*
const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  })); */


  import axios from 'axios';

// Request API.
axios
  .post('localhost:1337/filter-hotels', {
    "city":"jamshoro",
    "checkindate":"2022-01-14",
    "checkoutdate":"2022-01-15",
    "adult":1,
    "child":2
})
  .then(response => {
    // Handle success.
    console.log('Well done!');
    console.log('User profile', response.data.user);
    console.log('User token', response.data.jwt);
  })
  .catch(error => {
    // Handle error.
    console.log('An error occurred:', error.response);
  });


function Main() {

return(

 
        <Box>
        <Grid>
          <Header />

          </Grid>
          <Grid container spacing={2} >
          <Grid item xs={2}>
<Siderbar/>
</Grid>

<Grid item xs={10}>
   <Route/>

<Footer/>

</Grid>
</Grid>
</Box>


  
 );

}

export default Main;