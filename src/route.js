import React  from 'react';
import { BrowserRouter as Router,Routes, Route  } from 'react-router-dom';





// client page


import Landing from './pages/front/Landing';
import Listing from './pages/front/Listing';
import Details from './pages/front/Details';
import Booking from './pages/front/Booking';



// auth pages


import Login from './pages/auth/login';
import Register from './pages/auth/registor';

function App() {

   

return( 

    <div>






<Routes >


<Route exact  path="/Login" element={<Login/>}/>
<Route exact  path="/Registor" element={<Register/>}/>


<Route exact  path="/" element={<Landing/>}/>
<Route exact  path="/Hotel-Listing" element={<Listing/>}/>
<Route exact  path="/Hotel-Detail" element={<Details/>}/>
<Route exact  path="/Hotel-Booking" element={<Booking/>}/>



</Routes >

 


 
    </div>



);



}


export default App;